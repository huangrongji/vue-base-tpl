import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';
// import "./rem"
import http from './services';
import '@/assets/css/styles.css';

// 定义全局变量
Vue.prototype.$http = http;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => {
    return h(App);
  },
}).$mount('#app');

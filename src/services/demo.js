import request from '@/utils/request';
// import qs from "qs"

// const serverUrl = process.env.VUE_APP_BASE_API,
const serverUrl = 'https://api.baiyunairport.top',
  demo = {
    // GET请求
    list(params) {
      return request({
        url: `${serverUrl}/airport-business/listForWechat`,
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
        params,
      });
    },

    // POST请求
    weatherInfo(data) {
      return request({
        url: `${serverUrl}/flight-center/common/getWeatherInfo`,
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        data,
      });
    },
  };

export default demo;

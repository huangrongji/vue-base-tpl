# 基于Vue框架的项目模板

vue框架基础模板

```bash
├─.editorconfig
├─.env.development
├─.env.production
├─.eslintrc.js
├─.gitignore
├─.prettierrc.js
├─babel.config.js
├─commitlint.config.js // commit 日志规范
├─package.json
├─postcss.config.js
├─README.md
├─vue.config.js
├─yarn.lock
├─src
| ├─App.vue // 根组件
| ├─i18n.js // 多语言
| ├─main.js // 入口文件
| ├─rem.js
| ├─views // 页面
| | ├─About.vue
| | └Home.vue
| ├─utils
| | ├─index.js
| | └request.js // axios 请求封装
| ├─store
| | └index.js
| ├─services // 请求 Api
| | ├─index.js
| | └user.js
| ├─router
| | └index.js
| ├─mixins
| | └emitter.js
| ├─lang // 多语言配置
| | ├─en.json
| | └zh.json
| ├─components // 组件
| | ├─HelloI18n.vue
| | └HelloWorld.vue
| ├─assets // 静态资源
| | ├─logo.png
| | ├─css
| | | └styles.css
├─public
| ├─favicon.ico
| └index.html
```

const path = require('path');

module.exports = {
  publicPath: './',
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'zh',
      localeDir: 'lang',
      enableInSFC: true,
    },
  },
  chainWebpack: config => {
    // 修复HMR
    config.resolve.symlinks(true);
  },
  css: {
    // extract: false
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        img: path.resolve(__dirname, 'src/assets/images'),
      },
    },
  },
  devServer: {
    port: 9090,
    open: true,
    // 跨域代理
    // proxy: {
    //   '/api': {
    //     target: 'https://xxxxxx.com',
    //     changeOrigin: true,
    //     pathRewrite: { '^/api': '/api' },
    //   },
    // },
  },

  css: {
    loaderOptions: {
      scss: {
        // 向全局sass样式传入共享的全局变量, $src可以配置图片cdn前缀
        // 详情: https://cli.vuejs.org/guide/css.html#passing-options-to-pre-processor-loaders
        prependData: `
          @import '@/assets/css/variables.scss';
          `,
      },
    },
  },
};
